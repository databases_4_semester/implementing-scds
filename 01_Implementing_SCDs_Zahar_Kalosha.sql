-- Add new columns for SCD tracking
ALTER TABLE DimEmployee
ADD COLUMN start_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
ADD COLUMN end_date TIMESTAMP,
ADD COLUMN current_flag BOOLEAN DEFAULT TRUE;



-- Drop the old primary key and add a surrogate key
ALTER TABLE DimEmployee
DROP CONSTRAINT DimEmployee_pkey;

ALTER TABLE DimEmployee
ADD COLUMN employeeHistory_ID SERIAL PRIMARY KEY;

UPDATE  DimEmployee
SET employeeHistory_ID = DEFAULT;



-- Update existing data with initial values
UPDATE DimEmployee
SET start_date = HireDate, end_date = '9999-12-31 ';



-- Trigger function to handle updates
CREATE OR REPLACE FUNCTION dim_employees_update_trigger()
RETURNS TRIGGER AS $$
BEGIN
    IF (OLD.Title <> NEW.Title OR OLD.Address <> NEW.Address) AND OLD.current_flag AND NEW.current_flag THEN
        UPDATE DimEmployee
        SET end_date = current_timestamp,
            current_flag = FALSE,
			Title = OLD.Title,
			Address = OLD.Address
        WHERE EmployeeID = OLD.EmployeeID AND current_flag = TRUE;

      
        INSERT INTO DimEmployee (EmployeeID, LastName, FirstName, Title, BirthDate, HireDate, Address, City, Region, PostalCode, Country, HomePhone, Extension, start_date, end_date, current_flag)
        VALUES (OLD.EmployeeID, OLD.LastName, OLD.FirstName, NEW.Title, OLD.BirthDate, OLD.HireDate, NEW.Address, OLD.City, OLD.Region, OLD.PostalCode, OLD.Country, OLD.HomePhone, OLD.Extension, current_timestamp, '9999-12-31', TRUE);
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;



-- Create trigger on update
CREATE TRIGGER dim_employees_update
AFTER UPDATE ON DimEmployee
FOR EACH ROW
EXECUTE PROCEDURE dim_employees_update_trigger();

-- Prevent updates on the surrogate key
ALTER TABLE DimEmployee
DISABLE TRIGGER ALL;

ALTER TABLE DimEmployee
ADD CONSTRAINT check_employeehistory_id CHECK (employeeHistory_ID IS NOT NULL);

ALTER TABLE DimEmployee
ENABLE TRIGGER ALL;

--Chec of work
UPDATE DimEmployee SET Address ='Minsk' WHERE FirstName = 'John' and LastName ='Doe' AND current_flag =  True ;
UPDATE DimEmployee SET Title ='Vilnius' WHERE FirstName = 'Jane' and LastName ='Doe' AND current_flag =  True ;
   